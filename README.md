referenced from https://gist.github.com/bryanl/4337357<BR />
在 test branch 中，有一個 commit 同時具有 startService()/stopService() 及 bindService()/unbindService()<BR />
在使用 master branch 時，目前移除 start/stop, 只保留 bind/unbind<BR />
在畫面上請先 bind, 再透過 +1/+10 來觀察 Client(Activity) 叫 Server(Service) 改變量為 1/10<BR />
目前是採用 android studio 2.2 編譯，若有任何問題，請留言或來信討論<BR />
